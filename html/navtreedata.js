var NAVTREE =
[
  [ "Worms17", "index.html", [
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_after_turn_8c.html",
"_main_8h.html#a77cd5e7deab2fef3bb5cd06e67d2deed",
"extgraph_8h.html#a0e8e06711d447e37f548b9ea68322b27",
"extgraph_8h.html#a9a5f6a0ee936c25c68b9ab14e689200d",
"struct_t_t_a_r_c_h_i_v_e___e_n_t_r_y.html#a33d71f23ba2052d17f0b754dc35265b0"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';