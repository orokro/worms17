var _worms_8c =
[
    [ "spawnWorm", "_worms_8c.html#a1e44c7f8c928b52a5e571873da9a3631", null ],
    [ "Worm_spawnWorms", "_worms_8c.html#ac7dc943b773956934b61ac2dfe6bd38c", null ],
    [ "Worm_active", "_worms_8c.html#a34b5fb055616b1ead1fd4684353ede17", null ],
    [ "Worm_currentWorm", "_worms_8c.html#a235a1586a7b1c018b1bbf596293b367b", null ],
    [ "Worm_dir", "_worms_8c.html#a8852c77f3ffa25351a249ae4d571fbf8", null ],
    [ "Worm_health", "_worms_8c.html#a289b156e95e06cbfeeb5db72283a4c8a", null ],
    [ "Worm_isDead", "_worms_8c.html#a964f0194d6749ce9f1d613194d3912e3", null ],
    [ "Worm_mode", "_worms_8c.html#af68c60e1cf4f28a0ec237b3228763ee9", null ],
    [ "Worm_x", "_worms_8c.html#a834cfdd08e986879c56b9a821af55ee1", null ],
    [ "Worm_xVelo", "_worms_8c.html#a6fb32963ac0cbaa66ff8e15bdeb439fb", null ],
    [ "Worm_y", "_worms_8c.html#a47847e5e417476f8bd82422467b5760c", null ],
    [ "Worm_yVelo", "_worms_8c.html#aacdbd0725ce8d15f9d91bb05ca2acd04", null ]
];