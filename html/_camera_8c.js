var _camera_8c =
[
    [ "Camera_clearFocus", "_camera_8c.html#a7c75b4eb61c2cffcc59f61903619f10d", null ],
    [ "Camera_focusOn", "_camera_8c.html#aef25a4318644565219fa9153c3fa643b", null ],
    [ "Camera_update", "_camera_8c.html#a2e9caebd9db645e7361a1373de98dcb7", null ],
    [ "cameraIsFocused", "_camera_8c.html#a69b84c2da405138dda25d5ec3a2d44e2", null ],
    [ "cameraTargetX", "_camera_8c.html#ad04ea853cc90151237cb3f9bce8952fa", null ],
    [ "cameraTargetY", "_camera_8c.html#a193d317a63e1137928327d47b7db336b", null ],
    [ "camX", "_camera_8c.html#aca771de67ddcb728853aa418791c8900", null ],
    [ "camY", "_camera_8c.html#afc7df022bba054cf3a07d4a06b942083", null ],
    [ "userX", "_camera_8c.html#a95946c91770bc2514136b411b82531d5", null ],
    [ "userY", "_camera_8c.html#ab515d7a5985dbfa711f189f3106d1738", null ]
];