var searchData=
[
  ['cameraisfocused',['cameraIsFocused',['../_camera_8c.html#a69b84c2da405138dda25d5ec3a2d44e2',1,'Camera.c']]],
  ['cameratargetx',['cameraTargetX',['../_camera_8c.html#ad04ea853cc90151237cb3f9bce8952fa',1,'Camera.c']]],
  ['cameratargety',['cameraTargetY',['../_camera_8c.html#a193d317a63e1137928327d47b7db336b',1,'Camera.c']]],
  ['camx',['camX',['../_camera_8c.html#aca771de67ddcb728853aa418791c8900',1,'camX():&#160;Camera.c'],['../_main_8h.html#aca771de67ddcb728853aa418791c8900',1,'camX():&#160;Camera.c']]],
  ['camy',['camY',['../_camera_8c.html#afc7df022bba054cf3a07d4a06b942083',1,'camY():&#160;Camera.c'],['../_main_8h.html#afc7df022bba054cf3a07d4a06b942083',1,'camY():&#160;Camera.c']]],
  ['crate_5factive',['Crate_active',['../_crates_8c.html#a5229139895aec628e9cc8a7187f13cd7',1,'Crate_active():&#160;Crates.c'],['../_main_8h.html#a5229139895aec628e9cc8a7187f13cd7',1,'Crate_active():&#160;Crates.c']]],
  ['crate_5fhealth',['Crate_health',['../_crates_8c.html#a2d322dfea0620841b1c39572e96d43c6',1,'Crate_health():&#160;Crates.c'],['../_main_8h.html#a2d322dfea0620841b1c39572e96d43c6',1,'Crate_health():&#160;Crates.c']]],
  ['crate_5ftype',['Crate_type',['../_crates_8c.html#af8057702b429447344c8240dbd027408',1,'Crate_type():&#160;Crates.c'],['../_main_8h.html#af8057702b429447344c8240dbd027408',1,'Crate_type():&#160;Crates.c']]],
  ['crate_5fx',['Crate_x',['../_crates_8c.html#a6c660112989597df8f16f9a2a2dc02dd',1,'Crate_x():&#160;Crates.c'],['../_main_8h.html#a6c660112989597df8f16f9a2a2dc02dd',1,'Crate_x():&#160;Crates.c']]],
  ['crate_5fy',['Crate_y',['../_crates_8c.html#a8dff93bf23c85968a2de6a4d61a03df1',1,'Crate_y():&#160;Crates.c'],['../_main_8h.html#a8dff93bf23c85968a2de6a4d61a03df1',1,'Crate_y():&#160;Crates.c']]],
  ['csize_5fhi',['csize_hi',['../struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#af9ec8fb61fecad95ada5d776f5dd293b',1,'TTUNPACK_HEADER']]],
  ['csize_5flo',['csize_lo',['../struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a43d1e17e67ad4e67cd2f0232eb411983',1,'TTUNPACK_HEADER']]],
  ['cursorfastmove',['cursorFastMove',['../_cursor_8c.html#a942885b834c67001650a6189f0d7079b',1,'Cursor.c']]]
];
