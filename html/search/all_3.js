var searchData=
[
  ['camera_2ec',['Camera.c',['../_camera_8c.html',1,'']]],
  ['camera_5fclearfocus',['Camera_clearFocus',['../_camera_8c.html#a7c75b4eb61c2cffcc59f61903619f10d',1,'Camera_clearFocus():&#160;Camera.c'],['../_main_8h.html#a7c75b4eb61c2cffcc59f61903619f10d',1,'Camera_clearFocus():&#160;Camera.c']]],
  ['camera_5ffocuson',['Camera_focusOn',['../_camera_8c.html#aef25a4318644565219fa9153c3fa643b',1,'Camera_focusOn(short *targetX, short *targetY):&#160;Camera.c'],['../_main_8h.html#a9a53b82e7ecfd61bbf7b28dc148776b9',1,'Camera_focusOn(short *, short *):&#160;Camera.c']]],
  ['camera_5fupdate',['Camera_update',['../_camera_8c.html#a2e9caebd9db645e7361a1373de98dcb7',1,'Camera_update():&#160;Camera.c'],['../_main_8h.html#a2e9caebd9db645e7361a1373de98dcb7',1,'Camera_update():&#160;Camera.c']]],
  ['cameraisfocused',['cameraIsFocused',['../_camera_8c.html#a69b84c2da405138dda25d5ec3a2d44e2',1,'Camera.c']]],
  ['cameratargetx',['cameraTargetX',['../_camera_8c.html#ad04ea853cc90151237cb3f9bce8952fa',1,'Camera.c']]],
  ['cameratargety',['cameraTargetY',['../_camera_8c.html#a193d317a63e1137928327d47b7db336b',1,'Camera.c']]],
  ['camspeed',['camSpeed',['../_main_8h.html#ab5718ac9d5def1927bf4ba4d00bf436b',1,'Main.h']]],
  ['camx',['camX',['../_camera_8c.html#aca771de67ddcb728853aa418791c8900',1,'camX():&#160;Camera.c'],['../_main_8h.html#aca771de67ddcb728853aa418791c8900',1,'camX():&#160;Camera.c']]],
  ['camy',['camY',['../_camera_8c.html#afc7df022bba054cf3a07d4a06b942083',1,'camY():&#160;Camera.c'],['../_main_8h.html#afc7df022bba054cf3a07d4a06b942083',1,'camY():&#160;Camera.c']]],
  ['checkexplosions',['checkExplosions',['../_crates_8c.html#a666d1827597498a7c6d09613f02ead6e',1,'checkExplosions(short):&#160;Crates.c'],['../_mines_8c.html#a666d1827597498a7c6d09613f02ead6e',1,'checkExplosions(short):&#160;Mines.c'],['../_oil_drums_8c.html#a666d1827597498a7c6d09613f02ead6e',1,'checkExplosions(short):&#160;OilDrums.c']]],
  ['checkproximity',['checkProximity',['../_mines_8c.html#abbfc74cd00099432cdff23c015dc9d27',1,'Mines.c']]],
  ['cleargrayscreen',['ClearGrayScreen',['../extgraph_8h.html#ab011588c21f8980d21ac19a21ca1cbbc',1,'extgraph.h']]],
  ['cleargrayscreen2b',['ClearGrayScreen2B',['../extgraph_8h.html#a10b4777af68c79b30edbc4676b99dcbe',1,'extgraph.h']]],
  ['cleargrayscreen2b_5fr',['ClearGrayScreen2B_R',['../extgraph_8h.html#a3fe5e071b44108d5f10ae55005eb7dea',1,'extgraph.h']]],
  ['cleargrayscreen_5fr',['ClearGrayScreen_R',['../extgraph_8h.html#a2c3312a0e21e842b9889a03038aae7b0',1,'extgraph.h']]],
  ['clipsprite16_5fand_5fr',['ClipSprite16_AND_R',['../extgraph_8h.html#a8e9eacc61879ffa1e63efff8e5ae4f19',1,'extgraph.h']]],
  ['clipsprite16_5fblit_5fr',['ClipSprite16_BLIT_R',['../extgraph_8h.html#a4fefb0b3bad4e37763e96c3278d172d6',1,'extgraph.h']]],
  ['clipsprite16_5fmask_5fr',['ClipSprite16_MASK_R',['../extgraph_8h.html#aca92386ab0e8311f744eee97d16e973b',1,'extgraph.h']]],
  ['clipsprite16_5for_5fr',['ClipSprite16_OR_R',['../extgraph_8h.html#aad8ffadeb69251851d6afcdf3aaf3149',1,'extgraph.h']]],
  ['clipsprite16_5frplc_5fr',['ClipSprite16_RPLC_R',['../extgraph_8h.html#a9d17b739b6f4d48fe96ee15b03d057b1',1,'extgraph.h']]],
  ['clipsprite16_5fxor_5fr',['ClipSprite16_XOR_R',['../extgraph_8h.html#a619e7db2a3d7f0c85b60353a67a9af08',1,'extgraph.h']]],
  ['clipsprite32_5fand_5fr',['ClipSprite32_AND_R',['../extgraph_8h.html#a453eede20f60e4be53596b25ad5b685a',1,'extgraph.h']]],
  ['clipsprite32_5fblit_5fr',['ClipSprite32_BLIT_R',['../extgraph_8h.html#a12b8cfef93bd26e9844621a90d68790f',1,'extgraph.h']]],
  ['clipsprite32_5fmask_5fr',['ClipSprite32_MASK_R',['../extgraph_8h.html#a3fe2be6d83ffe3b3c9fc46915ff928b8',1,'extgraph.h']]],
  ['clipsprite32_5for_5fr',['ClipSprite32_OR_R',['../extgraph_8h.html#aa46a4a295840a8384e8c7d87f5832f00',1,'extgraph.h']]],
  ['clipsprite32_5frplc_5fr',['ClipSprite32_RPLC_R',['../extgraph_8h.html#aa54436f441f69924deb4114bc9deaeca',1,'extgraph.h']]],
  ['clipsprite32_5fxor_5fr',['ClipSprite32_XOR_R',['../extgraph_8h.html#a7d67bc87cd38d4613a8befe8f6b38b73',1,'extgraph.h']]],
  ['clipsprite8_5fand_5fr',['ClipSprite8_AND_R',['../extgraph_8h.html#a602175750cbccc0f713b18ee10fc29bc',1,'extgraph.h']]],
  ['clipsprite8_5fblit_5fr',['ClipSprite8_BLIT_R',['../extgraph_8h.html#a8793d1033d8a1d835e83e079eaefa96a',1,'extgraph.h']]],
  ['clipsprite8_5fmask_5fr',['ClipSprite8_MASK_R',['../extgraph_8h.html#ad616ba431ae8f333075b0c9f99aafc2b',1,'extgraph.h']]],
  ['clipsprite8_5for_5fr',['ClipSprite8_OR_R',['../extgraph_8h.html#ab109c8a3a0d992a56f424bc07f613155',1,'extgraph.h']]],
  ['clipsprite8_5frplc_5fr',['ClipSprite8_RPLC_R',['../extgraph_8h.html#a838169c5010373cc955e02f4c85adbef',1,'extgraph.h']]],
  ['clipsprite8_5fxor_5fr',['ClipSprite8_XOR_R',['../extgraph_8h.html#a3e0b380f1595c8d14998b05eb8f38376',1,'extgraph.h']]],
  ['color_5fblack',['COLOR_BLACK',['../extgraph_8h.html#a94e7c587ac22fa5a30a839d203b972d3a2a9daf215a30f1c539ead18c66380fc1',1,'extgraph.h']]],
  ['color_5fdarkgray',['COLOR_DARKGRAY',['../extgraph_8h.html#a94e7c587ac22fa5a30a839d203b972d3a1e583d53c3cf63c12a0a33e183914a04',1,'extgraph.h']]],
  ['color_5flightgray',['COLOR_LIGHTGRAY',['../extgraph_8h.html#a94e7c587ac22fa5a30a839d203b972d3aec97de9c2191dc549d9e99873851e30b',1,'extgraph.h']]],
  ['color_5fwhite',['COLOR_WHITE',['../extgraph_8h.html#a94e7c587ac22fa5a30a839d203b972d3ad47b4c240a0109970bb2a7fe3a07d3ec',1,'extgraph.h']]],
  ['comment_5fauthors',['COMMENT_AUTHORS',['../_main_general_8h.html#af93d8f0caba1069226df73ae364284b2',1,'MainGeneral.h']]],
  ['comment_5fbw_5ficon',['COMMENT_BW_ICON',['../_main_general_8h.html#a429a338b5a47bd9cd8948d9df5fc2bd7',1,'MainGeneral.h']]],
  ['comment_5fgray_5ficon',['COMMENT_GRAY_ICON',['../_main_general_8h.html#a2ed5932f86792ef5f29918b506fe87cc',1,'MainGeneral.h']]],
  ['comment_5fprogram_5fname',['COMMENT_PROGRAM_NAME',['../_main_general_8h.html#a08e2666002361d5f437b03f0ddc13dd9',1,'MainGeneral.h']]],
  ['comment_5fstring',['COMMENT_STRING',['../_main_general_8h.html#aea7db5c65fc41f20ef5e5db86fee9ad7',1,'MainGeneral.h']]],
  ['comment_5fversion_5fnumber',['COMMENT_VERSION_NUMBER',['../_main_general_8h.html#a071935ec169f3dcbd28d7d9b9994b77e',1,'MainGeneral.h']]],
  ['comment_5fversion_5fstring',['COMMENT_VERSION_STRING',['../_main_general_8h.html#a70b733d8c5b04a2fcc054e019d02e80a',1,'MainGeneral.h']]],
  ['crate_5factive',['Crate_active',['../_crates_8c.html#a5229139895aec628e9cc8a7187f13cd7',1,'Crate_active():&#160;Crates.c'],['../_main_8h.html#a5229139895aec628e9cc8a7187f13cd7',1,'Crate_active():&#160;Crates.c']]],
  ['crate_5fhealth',['Crate_health',['../_crates_8c.html#a2d322dfea0620841b1c39572e96d43c6',1,'Crate_health():&#160;Crates.c'],['../_main_8h.html#a2d322dfea0620841b1c39572e96d43c6',1,'Crate_health():&#160;Crates.c']]],
  ['crate_5ftype',['Crate_type',['../_crates_8c.html#af8057702b429447344c8240dbd027408',1,'Crate_type():&#160;Crates.c'],['../_main_8h.html#af8057702b429447344c8240dbd027408',1,'Crate_type():&#160;Crates.c']]],
  ['crate_5fx',['Crate_x',['../_crates_8c.html#a6c660112989597df8f16f9a2a2dc02dd',1,'Crate_x():&#160;Crates.c'],['../_main_8h.html#a6c660112989597df8f16f9a2a2dc02dd',1,'Crate_x():&#160;Crates.c']]],
  ['crate_5fy',['Crate_y',['../_crates_8c.html#a8dff93bf23c85968a2de6a4d61a03df1',1,'Crate_y():&#160;Crates.c'],['../_main_8h.html#a8dff93bf23c85968a2de6a4d61a03df1',1,'Crate_y():&#160;Crates.c']]],
  ['cratehealth',['crateHealth',['../_main_8h.html#af70522def23a8c273e1263f06dd15c30',1,'Main.h']]],
  ['crates_2ec',['Crates.c',['../_crates_8c.html',1,'']]],
  ['crates_5fspawncrate',['Crates_spawnCrate',['../_crates_8c.html#af1f0877a91de9767e8d49bef5e7bbcd5',1,'Crates_spawnCrate(char type):&#160;Crates.c'],['../_main_8h.html#afb32127e7800f4a63b8bf438c689c256',1,'Crates_spawnCrate(char):&#160;Crates.c']]],
  ['crates_5fupdate',['Crates_update',['../_crates_8c.html#a384991346da52c456de16298545aac75',1,'Crates_update():&#160;Crates.c'],['../_main_8h.html#a384991346da52c456de16298545aac75',1,'Crates_update():&#160;Crates.c']]],
  ['cratetool',['crateTool',['../_main_8h.html#aa503b6c3d3101967a452d36f158e9821',1,'Main.h']]],
  ['crateweapon',['crateWeapon',['../_main_8h.html#a278307d0ff93da188bbc0ee54c67ca33',1,'Main.h']]],
  ['createispriteishadow16_5fr',['CreateISpriteIShadow16_R',['../extgraph_8h.html#a365018683e953e065e5baf16f52b6de0',1,'extgraph.h']]],
  ['createispriteishadow32_5fr',['CreateISpriteIShadow32_R',['../extgraph_8h.html#a5ee9e8743c976f72f1a9e3067840074b',1,'extgraph.h']]],
  ['createispriteishadow8_5fr',['CreateISpriteIShadow8_R',['../extgraph_8h.html#aaede68a607a9154fe777d2901423dcb4',1,'extgraph.h']]],
  ['createispriteshadow16_5fr',['CreateISpriteShadow16_R',['../extgraph_8h.html#aa679d8c4601ed8cb99084edb0ddb0b49',1,'extgraph.h']]],
  ['createispriteshadow32_5fr',['CreateISpriteShadow32_R',['../extgraph_8h.html#a835ec030b8c03ca6bff2296e69f4ae72',1,'extgraph.h']]],
  ['createispriteshadow8_5fr',['CreateISpriteShadow8_R',['../extgraph_8h.html#abcdccc737372f329d915c8a53f3455f3',1,'extgraph.h']]],
  ['createspriteishadow16_5fr',['CreateSpriteIShadow16_R',['../extgraph_8h.html#ae0d83b96ab6eac20e73e55813d6c42fc',1,'extgraph.h']]],
  ['createspriteishadow32_5fr',['CreateSpriteIShadow32_R',['../extgraph_8h.html#abd268b6ff77efb3fd8f67b6bcff3f4a9',1,'extgraph.h']]],
  ['createspriteishadow8_5fr',['CreateSpriteIShadow8_R',['../extgraph_8h.html#ab64b685e00b1fe3feb55cb439cd1b6c1',1,'extgraph.h']]],
  ['createspriteishadowx8_5fr',['CreateSpriteIShadowX8_R',['../extgraph_8h.html#a96353144b1f69737067ff6fdbbcabf25',1,'extgraph.h']]],
  ['createspriteshadow16_5fr',['CreateSpriteShadow16_R',['../extgraph_8h.html#a5edb1783b2b8b0ae7d89dd9118609496',1,'extgraph.h']]],
  ['createspriteshadow32_5fr',['CreateSpriteShadow32_R',['../extgraph_8h.html#ad5cf29db8186f843eb53a3e5899e5ec2',1,'extgraph.h']]],
  ['createspriteshadow8_5fr',['CreateSpriteShadow8_R',['../extgraph_8h.html#a2cb8d74971f757577fec73cfdb763bf1',1,'extgraph.h']]],
  ['createspriteshadowx8_5fr',['CreateSpriteShadowX8_R',['../extgraph_8h.html#a40fcbefd6c4f09c2cd45be7e1454a510',1,'extgraph.h']]],
  ['csize_5fhi',['csize_hi',['../struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#af9ec8fb61fecad95ada5d776f5dd293b',1,'TTUNPACK_HEADER']]],
  ['csize_5flo',['csize_lo',['../struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a43d1e17e67ad4e67cd2f0232eb411983',1,'TTUNPACK_HEADER']]],
  ['cursor_2ec',['Cursor.c',['../_cursor_8c.html',1,'']]],
  ['cursor_5fenter',['Cursor_enter',['../_game_8c.html#a15be5b5bc020f2079a57fbb64deed61d',1,'Cursor_enter():&#160;Cursor.c'],['../_cursor_8c.html#a15be5b5bc020f2079a57fbb64deed61d',1,'Cursor_enter():&#160;Cursor.c']]],
  ['cursor_5fexit',['Cursor_exit',['../_game_8c.html#a65fafee9e5e89df657b3086556d42d28',1,'Cursor_exit():&#160;Cursor.c'],['../_cursor_8c.html#a65fafee9e5e89df657b3086556d42d28',1,'Cursor_exit():&#160;Cursor.c']]],
  ['cursor_5fupdate',['Cursor_update',['../_game_8c.html#a94b4fb6b8b459c5525c9ae20cf3dfeed',1,'Cursor_update():&#160;Cursor.c'],['../_cursor_8c.html#a94b4fb6b8b459c5525c9ae20cf3dfeed',1,'Cursor_update():&#160;Cursor.c']]],
  ['cursorfastmove',['cursorFastMove',['../_cursor_8c.html#a942885b834c67001650a6189f0d7079b',1,'Cursor.c']]]
];
